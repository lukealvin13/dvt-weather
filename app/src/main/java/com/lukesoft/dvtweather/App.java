package com.lukesoft.dvtweather;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;


import com.lukesoft.dvtweather.di.module.ApiModule;
import com.splunk.mint.Mint;


public class App extends MultiDexApplication {
    private static App instance;
    private AppComponent appComponent;

    public App() {
        instance = this;
    }

    public static App get(Context context){
        return (App)context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Mint.initAndStartSession(this, "a5731dcb");
        appComponent= DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static App getInstance() {
        return instance;
    }
}

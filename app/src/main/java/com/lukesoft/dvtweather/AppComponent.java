package com.lukesoft.dvtweather;


import com.lukesoft.dvtweather.di.module.ActivityModule;
import com.lukesoft.dvtweather.di.module.ApiModule;
import com.lukesoft.dvtweather.data.source.remote.WeatherRemoteDataSource;
import com.lukesoft.dvtweather.presenter.WeatherPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class, ActivityModule.class})
public interface AppComponent {
    App getApplication();

    AppConfig providesAppConfig();

    WeatherRemoteDataSource.WeatherService weatherService();
    WeatherRemoteDataSource WeatherRemoteDataSource();


    void inject(WeatherRemoteDataSource weatherRemoteDataSource);
    void inject(WeatherPresenter weatherPresenter);
}

package com.lukesoft.dvtweather;



import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule {

    private App application;


    AppModule(App application){
        this.application=application;
    }

    @Provides
    @Singleton
    App provideApplication(){
        return application;
    }


}

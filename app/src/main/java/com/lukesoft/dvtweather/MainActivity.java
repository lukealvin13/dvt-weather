package com.lukesoft.dvtweather;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import com.lukesoft.dvtweather.contract.WeatherContract;
import com.lukesoft.dvtweather.data.Main;
import com.lukesoft.dvtweather.data.WeatherResponse;
import com.lukesoft.dvtweather.di.component.DaggerActivityComponent;
import com.lukesoft.dvtweather.di.module.ActivityModule;
import com.squareup.picasso.Picasso;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationParams;

import static io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider.REQUEST_CHECK_SETTINGS;

public class MainActivity extends AppCompatActivity implements WeatherContract.View{
    @BindView(R.id.txtDate) TextView txtCurrentDate;
    @BindView(R.id.txtLocation) TextView txtLocation;
    @BindView(R.id.txtMax) TextView txtMax;
    @BindView(R.id.txtMin) TextView txtMin;
    @BindView(R.id.imgIcon) ImageView imgIcon;


    private static final int LOCATION_PERM_REQUEST_CODE = 15000;


    @Inject
    ProgressDialog mProgressDialog;

    @Inject
    LovelyStandardDialog mLovelyStandardDialog;

    @Inject
    WeatherContract.Presenter mPresenter;

    @Inject
    AppConfig mAppConfig;
    private Location mCurrentLocation;
    private boolean isPermitted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .appComponent(App.getInstance().getAppComponent())
                .activityModule(new ActivityModule(this))
                .build().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPresenter.setView(this);

        getLocation();


    }


    /**
     * Checks if location services are enabled on the device
     * @return
     */
    private boolean isLocationServicesEnabled(){
        return SmartLocation.with(this)
                .location().state()
                .locationServicesEnabled();
    }

    private void createLocationRequest() {
        final String TAG="locationRequest";
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                new LovelyStandardDialog(MainActivity.this)
                                .setTopColorRes(R.color.dialogWarning)
                                .setTitle("Location Settings")
                                .setMessage("Please make sure your location settings are enabled on your device.")
                                .setPositiveButton("Try Again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getLocation();
                                    }
                                }).setNegativeButton("Cancel", null)
                                .show();
                        break;
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                if (resultCode == Activity.RESULT_OK) {
                    getLocation();
                } else {
                    finish();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERM_REQUEST_CODE) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        mLovelyStandardDialog.setTopColorRes(R.color.dialogWarning)
                                .setTitle("Permissions Required")
                                .setCancelable(false)
                                .setMessage("You have forcefully denied some of the required permissions " +
                                        "for this app. Please open settings, go to permissions and allow them.")
                                .setPositiveButton("Settings", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                    Uri.fromParts("package", getPackageName(), null));
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                ).setNegativeButton("Cancel", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        finish();
                                    }
                                })
                                .show();
                    } else {
                        mLovelyStandardDialog.setTopColorRes(R.color.dialogWarning)
                                .setTitle("Permission Error")
                                .setCancelable(false)
                                .setMessage("Please grant us permission to access your current location")
                                .setPositiveButton("Grant Permission", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        getLocation();
                                    }
                                }).setNegativeButton("Cancel", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            finish();
                                        }
                                    })
                                .show();
                    }
                }
            }

            if (isPermitted){
                getLocation();
            }

        }
    }


    private void getLocation() {

        if (isLocationPermissionGranted()){
            if (!isLocationServicesEnabled()){
                createLocationRequest();
                return;
            }
            mProgressDialog.setMessage("Fetching your location.");
            mProgressDialog.show();

            SmartLocation.with(this).location().config(LocationParams.NAVIGATION)
                    .oneFix()
                    .start(new OnLocationUpdatedListener() {


                        @Override
                        public void onLocationUpdated(Location location) {
                            mProgressDialog.dismiss();
                            MainActivity.this.mCurrentLocation = location;
                            getCurrentWeather();

                        }
                    });
        }

    }

    private boolean isLocationPermissionGranted(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERM_REQUEST_CODE);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    private void getCurrentWeather(){
        Map<String,Object> queryMap =  new HashMap<>();
        queryMap.put("lat",mCurrentLocation.getLatitude());
        queryMap.put("lon",mCurrentLocation.getLongitude());
        queryMap.put("appid", mAppConfig.getAPIkey());
        queryMap.put("units", "metric");
        mPresenter.fetch(queryMap);
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active){
            mProgressDialog.setMessage("Fetching the weather..");
            mProgressDialog.show();
        }else{
            mProgressDialog.dismiss();
        }

    }

    @Override
    public void onSuccess(WeatherResponse response) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy",new Locale("en_ZAR"));
        Main currentMain = response.getMain();
        txtLocation.setText(String.format(getString(R.string.unknown_address),Double.toString(mCurrentLocation.getLatitude()), Double.toString(mCurrentLocation.getLongitude())));


        txtCurrentDate.setText("Today, "+df.format(c.getTime()));
        txtMin.setText(String.format(getString(R.string.min_temp),Double.toString(currentMain.getTempMin())));
        txtMax.setText(String.format(getString(R.string.max_temp),Double.toString(currentMain.getTempMax())));
        if ((response.getWeather()!=null) && (response.getWeather().size()>0)){
            Picasso.with(this).load("http://openweathermap.org/img/w/"+response.getWeather().get(0).getIcon()+".png").placeholder(R.drawable.loading).into(imgIcon);
        }


        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String country = addresses.get(0).getCountryName();
            txtLocation.setText(String.format(getString(R.string.location),address, city, country));
//
        } catch (IOException e) {
            e.printStackTrace();
//            txtLocation.setText(getString(R.string.unknown_address));
        }





        //Display the weather
        Log.i("theweather", currentMain.toString());
    }

    @Override
    public void onFail(String reason) {
        mLovelyStandardDialog.setTopColorRes(R.color.dialogWarning)
                .setTitle("Failed")
                .setCancelable(false)
                .setMessage(reason)
                .setPositiveButton("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getCurrentWeather();
                    }
                }).setNegativeButton("Cancel", null)
                .show();
    }

    @Override
    public boolean isActive() {
        return true;
    }

}

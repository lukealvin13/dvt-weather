package com.lukesoft.dvtweather.contract;
import com.lukesoft.dvtweather.data.Main;
import com.lukesoft.dvtweather.data.WeatherResponse;

import java.util.Map;

/**
 * Created by lukemadzedze on 2017/07/31.
 */

public interface WeatherContract {
    interface View{
        void setLoadingIndicator(boolean active);
        void onSuccess(WeatherResponse weatherResponse);
        void onFail(String reason);
        boolean isActive();
    }

    interface Presenter{
        void fetch(Map<String, Object> params);
        void setView(WeatherContract.View mView);
    }
}

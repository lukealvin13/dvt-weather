/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lukesoft.dvtweather.data.source.remote;

import com.lukesoft.dvtweather.App;
import com.lukesoft.dvtweather.AppComponent;
import com.lukesoft.dvtweather.data.WeatherResponse;
import java.util.Map;
import javax.inject.Inject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public class WeatherRemoteDataSource {
    @Inject
    WeatherService mWeatherService;

    private static WeatherRemoteDataSource INSTANCE;

    public static WeatherRemoteDataSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRemoteDataSource();
        }
        App.getInstance().getAppComponent().inject(INSTANCE);
        return INSTANCE;
    }

    // Prevent direct instantiation.
    private WeatherRemoteDataSource() {}

    public void fetch(Map<String, Object> params, Callback<WeatherResponse> callback) {
        Call<WeatherResponse> call = mWeatherService.fetch(params);
        call.enqueue(callback);
    }

    public interface WeatherService {
        @GET("weather")
        Call<WeatherResponse> fetch(@QueryMap Map<String, Object> params);
    }


}

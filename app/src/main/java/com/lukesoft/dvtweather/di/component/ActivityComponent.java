package com.lukesoft.dvtweather.di.component;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.widget.ProgressBar;

import com.lukesoft.dvtweather.AppComponent;
import com.lukesoft.dvtweather.MainActivity;
import com.lukesoft.dvtweather.contract.WeatherContract;
import com.lukesoft.dvtweather.di.module.ActivityModule;
import com.lukesoft.dvtweather.di.scope.ActivityScope;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import dagger.Component;

@ActivityScope
@Component(modules =ActivityModule.class,dependencies = AppComponent.class)
public interface ActivityComponent {


    Context activityContext();
    ProgressDialog progressDialog();
    LovelyStandardDialog lovelyStandardDialog();
    LovelyProgressDialog lovelyProgressDialog();
    LovelyInfoDialog lovelyInfoDialog();
    Activity getActivity();

    WeatherContract.Presenter presenter();

    LovelyTextInputDialog mLovelyTextInputDialog();

    void inject(MainActivity mainActivity);

}

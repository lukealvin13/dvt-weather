package com.lukesoft.dvtweather.di.module;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.lukesoft.dvtweather.R;
import com.lukesoft.dvtweather.contract.WeatherContract;
import com.lukesoft.dvtweather.di.scope.ActivityScope;
import com.lukesoft.dvtweather.presenter.WeatherPresenter;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    final AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        mActivity = activity;
    }


    @Provides
    @ActivityScope
    public Activity activity() {
        return mActivity;
    }


    @Provides
    @ActivityScope
    public Context activityContext() {
        return mActivity;
    }

    //Utilities

    @Provides
    @ActivityScope
    public LovelyStandardDialog lovelyStandardDialog(){
        return new LovelyStandardDialog(mActivity)
                .setTopColorRes(R.color.dialogError);
    }

    @Provides
    @ActivityScope
    public LovelyTextInputDialog mLovelyTextInputDialog(){
        return new LovelyTextInputDialog(mActivity)
                .setTopColorRes(R.color.black);
    }


    @Provides
    @ActivityScope
    public LovelyProgressDialog lovelyProgressDialog(){
        return new LovelyProgressDialog(mActivity)
                .setTopColorRes(R.color.teal);
    }

    @Provides
    @ActivityScope
    public LovelyInfoDialog lovelyInfoDialog(){
        return new LovelyInfoDialog(mActivity)
                .setTopColorRes(R.color.dialogError);
    }

    @Provides
    @ActivityScope
    public FragmentManager fragmentManager() {
        return mActivity.getSupportFragmentManager();
    }


    @Provides
    @ActivityScope
    public ProgressDialog progressDialog(){
        return new ProgressDialog(mActivity);
    }

    @Provides
    @ActivityScope
    public WeatherContract.Presenter presenter(){
        return WeatherPresenter.getInstance();
    }


}

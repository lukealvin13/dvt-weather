package com.lukesoft.dvtweather.di.module;


import com.google.gson.GsonBuilder;
import com.lukesoft.dvtweather.AppConfig;
import com.lukesoft.dvtweather.data.source.remote.WeatherRemoteDataSource;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    AppConfig providesAppConfig(){
        return new AppConfig();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {

        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, AppConfig providesAppConfig) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(providesAppConfig.getAPIBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation() .create()));
        return builder.client(client).build();
    }

    @Provides
    @Singleton
    public WeatherRemoteDataSource.WeatherService weatherService(Retrofit retrofit){
        return retrofit.create(WeatherRemoteDataSource.WeatherService.class);
    }

    @Provides
    @Singleton
    public WeatherRemoteDataSource weatherRemoteDataSource(){
        return WeatherRemoteDataSource.getInstance();
    }

}

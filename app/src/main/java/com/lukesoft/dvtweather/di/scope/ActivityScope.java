package com.lukesoft.dvtweather.di.scope;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}

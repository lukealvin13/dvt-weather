package com.lukesoft.dvtweather.presenter;

import com.lukesoft.dvtweather.App;
import com.lukesoft.dvtweather.contract.WeatherContract;
import com.lukesoft.dvtweather.data.WeatherResponse;
import com.lukesoft.dvtweather.data.source.remote.WeatherRemoteDataSource;

import java.util.Map;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lukemadzedze on 2017/08/10.
 */

public class WeatherPresenter implements WeatherContract.Presenter {
    private WeatherContract.View mView;

    @Inject
    WeatherRemoteDataSource mWeatherRemoteDataSource;

    private static WeatherPresenter INSTANCE;

    private WeatherPresenter() {}

    public static WeatherContract.Presenter getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new WeatherPresenter();
        }
        App.getInstance().getAppComponent().inject(INSTANCE);

        return INSTANCE;
    }

    @Override
    public void fetch(Map<String, Object> params) {
        if (this.mView.isActive()){
            mView.setLoadingIndicator(true);
        }

        mWeatherRemoteDataSource.fetch(params, new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> res) {
                if (mView.isActive()){
                    mView.setLoadingIndicator(false);
                    WeatherResponse response = res.body();
                    if (response!=null){
                        mView.onSuccess(response);
                    }else{
                        mView.onFail("Invalid Response form the server.");
                    }

                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                if (mView.isActive()){
                    mView.setLoadingIndicator(false);
                }
                mView.onFail("Connectivity Error, Please make sure you are connected to the internet.");
            }
        });
    }

    @Override
    public void setView(WeatherContract.View mView) {
        this.mView = mView;
    }

}
